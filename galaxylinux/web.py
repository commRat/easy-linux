from importlib.metadata import distributions
import os
import sys
import flask
from flask import request
import galaxy


basedir = os.path.dirname(sys.executable if getattr(sys, 'frozen', False) else __file__)
app = flask.Flask(__name__, template_folder=os.path.join(basedir, 'templates'), static_folder=os.path.join(basedir, 'static'))
port_ = None


@app.route('/disks')
def get_disks():
    return flask.jsonify(galaxy.get_disks_names())


@app.route('/distributions', methods=['GET'])
def distributions():
    return flask.jsonify(galaxy.get_distributions())


@app.route('/connect', methods=['POST'])
def connection_create():
    connection_details = request.get_json()
    return flask.jsonify(galaxy.connection_create(connection_details))


@app.route('/confirm', methods=['GET'])
def connection_confirm():
    return flask.jsonify(galaxy.connection_confirm())


@app.route("/")
def index():
    return "<p>Galaxy Linux</p>"


def start():
    app.run()
